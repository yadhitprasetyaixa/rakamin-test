import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"


const Contact = () => {
    return (
        <div className="flex flex-col justify-center w-full h-full">
            <div className="flex justify-center pb-5">
                <h1 className="text-2xl font-bold">Found me!</h1>
            </div>
            <div className="flex flex-col justify-center items-center">
                <div className="text-xl">
                    Linkedin
                </div>
                <div className="text-xl ">
                    Instagram
                </div>
                <div className="text-xl">
                    Line
                </div>
            </div>

        </div>
    )
}

export default Contact